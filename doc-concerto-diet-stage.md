# Concerto-DIET
Documentation DIET :
Documentation Concerto :

## Principes de bases

Concerto est utilisé pour décrire les étapes/fonctionnement/dépendences des composant de DIET.

Chaque Agent de DIET (SeD compris, client à faire), à une classe attribué en python qui hérite des composants de Concerto.
Ces classes sont adaptés au fonctionnement (non universel) des composants de DIET pour faciliter la reconfiguration dynamique de la hierarchie déployé.

Pour lancer un test : `python3 test_gen_assembly_diet.py`.
Il vous faut bien sûr au préalable avoir installé DIET (penser à rajouter la librairie à la variable voulu) et Concerto (rajouter le répertoire de celui-ci au pythonPATH).

## "Custom API"

"Custom API" permet de lancer les commandes sur des machines distantes si besoin et de synchronizer les fichiers de configurations.
Cette API n'est pas exclusif à DIET, ni même à Concerto, même si celle-ci a été developpé pour fonctionner avec ces deux logiciels.




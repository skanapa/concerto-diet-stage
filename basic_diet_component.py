import os
import stat
from string import Template

from concerto.all import *
from examples.utils import *

class BasicDietComponent(Component):
	def __init__(self, name, hostname, trace_level, verbose):
		super().__init__()
		self.name = name
		self.hostname = hostname
		self.trace_level = trace_level
		self.verbose = verbose
		self.properties = None
		self.cmd = None
		self.diet_install_path = "/usr/local/bin"
		self.diet_lib = "export LD_LIBRARY_PATH='/usr/local/lib'"
		self.working_path = "/tmp"
		self.process = None
		

	def write_config(self, config_string):
		self.config_path = self.path + '/config'
		if os.path.isdir(self.path) == False:
			print(self.name + " :: ERROR" + self.path + "Isn't a directory.")
			quit()

		if self.verbose > 4:
			self.print_color(" :: Writing config to file.")

		if self.verbose > 5:
			self.print_color(" :: config file path : " + self.config_path)
			self.print_color(" :: config :\n" + config_string)

		config_file = open(self.config_path, 'w')
		config_file.write(config_string)
		config_file.close()
		

	def get_config_file(self):
		return self.config_path


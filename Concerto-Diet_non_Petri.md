# ""Réseau de Petri"" Concerto-Diet
* omniNames
    * Places :
        * Installed
        * Configured
        * Running
    * Behaviors :
        * Configuration (Installed -> Configured)
        * Run (Configured -> Running)
        * Shutdown (Running -> Configured)
* Master Agent
    * Places :
        * Installed
        * Configured
        * Running
    * Behaviors :
        * Configuration (Installed -> Configured)
        * Run (Configured -> Running) *dep : omniNames.Running*
        * Shutdown (Running -> Configured)
* Local Agent
    * Places :
        * Installed
        * Configured
        * Deconnected
        * Connected
    * Behaviors :
        * Configuration (Installed -> Configured)
        * Run (Configured -> Deconnected)
        * Shutdown (Deconnected -> Configured)
        * Connection (Deconnected -> Connected) *dep : Parent-Agent.Running*
        * Deconnection (Connected -> Deconnected)
* SeD
    * Places :
        * Installed
        * Configured
        * Deconnected
        * Connected
    * Behaviors :
        * Configuration (Installed -> Configured)
        * Run (Configured -> Deconnected)
        * Shutdown (Deconnected -> Configured)
        * Connection (Deconnected -> Connected) *dep : Parent-Agent.Running*
        * Deconnection (Connected -> Deconnected)
* Client
    * Places :
        * Installed
        * Configured
        * Running
    * Behaviors :
        * Configuration (Installed -> Configured)
        * Run (Configured -> Running) *dep : SeD.Running*
        * Shutdown (Running -> Configured)
